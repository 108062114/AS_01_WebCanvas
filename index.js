var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var cursors = [
  'pen_cursor.png',
  'eraser_cursor.png'
];

let x = 0;
let y = 0;
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.offsetX,
    y: evt.offsetY
  };
};
canvas.addEventListener('mousedown', function (evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(mousePos.x, mousePos.y + 32);
  canvas.addEventListener('mousemove', mouseMove, false);
  
});
function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y + 32);
  ctx.stroke();
};
canvas.addEventListener('mouseup', function () {
  
  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);


document.getElementById('penswidth').value = 10;
ctx.lineWidth = 5;

function brush() {
  ctx.strokeStyle = 'black';
  canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function eraser() {
  ctx.strokeStyle = canvas.style.background;
  canvas.style.cursor = 'url("' + cursors[1] + '"), default';
}

function reset() {
  window.location.reload(); 
}


function changeWidth() {
  ctx.lineWidth = document.getElementById('penswidth').value;
}
function save (formatType) {
  const dataURL = canvas.toDataURL({
    format: `.png`,
    top: 0,
    left: 0,
    width: 850,
    height: 650,
    multiplier: 1.0,
    quality: 0.1
  })
  const a = document.createElement('a')
  a.href = dataURL
  a.download = `output.png`
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
}
